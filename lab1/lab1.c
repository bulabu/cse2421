#include <stdio.h>

int main(int argc, char* argv[]) {
	int i;
	int positionChoice;
	int positionToOutput = 35;
	int x[10000];

	for(i = 0; i < 10000; ++i){
		x[i] = i+20;
	}

	printf("Which array value do you want to see? Choose a number between 1 and 9999:\n ");
	scanf("%d", &positionChoice);

	printf("Value at position %d is %d\n\n", positionChoice, x[positionChoice]);
	printf("Value at position %d is %d\n", positionToOutput, x[positionToOutput]);
}
