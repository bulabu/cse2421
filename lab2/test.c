#include <stdio.h>
#define MAX_LINE 256
void main(){
	int count;
	char content[MAX_LINE];
	int i;
	int innerCounter = 0;

	scanf("%d", &count);

	for(i=0; i<count; i++){
		while((content[innerCounter] = getchar()) != '\n' && content[innerCounter] != EOF){
			innerCounter++;
			continue;
		}
	}

	printf("%s", content);
}
